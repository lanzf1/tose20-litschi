package tose20litschi.smartpoll;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tose20litschi.smartpoll.web.SmartPollIndexController;
import tose20litschi.sparkbase.H2SparkApp;
import tose20litschi.sparkbase.UTF8ThymeleafTemplateEngine;

import java.util.Map.Entry;

import static spark.Spark.get;
import static spark.Spark.post;

public class SmartPollServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SmartPollServer.class);

	public static void main(String[] args) {

		for(Entry<Object, Object> property: System.getProperties().entrySet()) {
			log.debug(String.format("Property %s : %s", property.getKey(),property.getValue()));
		}

		SmartPollServer server = new SmartPollServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {
		get("/", new SmartPollIndexController(), new UTF8ThymeleafTemplateEngine());
	}

}
